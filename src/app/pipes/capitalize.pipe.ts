import { Pipe, PipeTransform } from '@angular/core';
//Ejemplo si un usuario pone el nombre en minúscila que lo coloque siempre en Mayúsculas
@Pipe({
	name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {
	transform(value: string): any {
		let finalString = '';
		for (let str of value.split(' ')) {
			finalString += str.trim().charAt(0).toUpperCase() + str.slice(1)+ ' ';
		}
		return finalString;
	}
}


//Añadire en app.module.ts el CapitalizePipe
// y el pipe | donde lo quiera mostrar. en nuestro caso en detalle y en Listcontact (linea 27)