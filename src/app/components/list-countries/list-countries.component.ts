import { Component, OnInit } from '@angular/core';
import { CountryService } from 'src/app/services/country.service';
import { forkJoin } from 'rxjs';
import { Country } from 'src/app/models/country';
import { Region } from 'src/app/models/region';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import * as l from 'lodash';
@Component({
  selector: 'app-list-countries',
  templateUrl: './list-countries.component.html',
  styleUrls: ['./list-countries.component.scss']
})
export class ListCountriesComponent implements OnInit {

  public listRegions: Region[];
  public listCountries: Country[];
  public listCountriesToVisit: Country[];
  public load: boolean;
  public regionSelected: string;
  constructor(private countryService: CountryService) {
    // this.load = false;
    this.listCountries = [];
    this.listCountriesToVisit = [];
    this.regionSelected = 'EU';
  }

  ngOnInit() {

    forkJoin([
      this.countryService.getCountriesByRegion("eu"),
      this.countryService.getAllRegions()
    ]
    ).subscribe(results => {
      this.listCountries = results[0];
      this.listRegions = results[1];
      // this.load = false;
      console.log("Soy el list de Paises en eu", results);
      console.log("Soy el list de Paises", this.listCountries);
      console.log("Soy el list de Regiones", this.listRegions);
    }), error => {
      console.error('Error' + error);
      // this.load = true;

    }


    // this.countryService.getCountriesByRegion("eu").subscribe(list => {
    //   console.log("Soy el list de Paises en eu", list);
    // })
    // this.countryService.getAllRegions().subscribe(list => {
    //   console.log("Regions:", list);
    // })


  }

  filterCountries($event) {
    console.log($event);
    this.load = true;
    this.countryService.getCountriesByRegion($event.value).subscribe(list => {
      this.listCountries = l.differenceBy(list,this.listCountriesToVisit, c => c.name);
    this.load = false;
    })

  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

}
