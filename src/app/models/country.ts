import { ICountry } from "../interfaces/Icountry";
import * as l from 'lodash';

export class Country implements ICountry {

	constructor(data) {
		l.set(this, 'data', data);
	}

	get name(): string {
		return l.get(this, 'data.name')
	}
	get flag(): string {
		return l.get(this, 'data.flag')
	}
}