import { IRegion } from "./../interfaces/iregion";
import * as l from 'lodash';

export class Region implements IRegion {

	constructor(data) {
		l.set(this, 'data', data);
	}

	get code(): string {
		return l.get(this, 'data.code')
	}
	get name(): string {
		return l.get(this, 'data.name')
	}
}