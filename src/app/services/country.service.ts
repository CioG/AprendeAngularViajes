import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICountry } from '../interfaces/Icountry';
import { IRegion } from '../interfaces/iregion';
import { Country } from '../models/country';
import { Region } from '../models/region';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient) { }

  //                             devuelve un observable de any
  getCountriesByRegion(region): Observable<ICountry[]> {
    return this.http.get<ICountry[]>('https://restcountries.eu/rest/v2/regionalbloc/' + region).pipe(

      // map(data => data) Con esto traigo toda la data, añadiendole el map y referenciandolo a "d", nos trae la data con el formato
      map(data => data.map(d => new Country(d)))

    )
  }

  getAllRegions(): Observable<IRegion[]> {
    return this.http.get<IRegion[]>('assets/data/regions.json').pipe(
      map(data => data.map(d => new Region(d)))
    )
  }

}
